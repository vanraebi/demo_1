<?php declare(strict_types = 1);

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

class DeploymentConfigurationCommand extends Command
{
    protected static $defaultName = 'app:create-deployment';

    protected function configure(): void
    {
        $this->setDescription('Create new deployment');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Deployment Creator',
            '=================',
            ''
                         ]);
        $fileSystem = new Filesystem();
        $fileSystem->touch('.mage.yml');
        $fileSystem->touch('.git_test.yml');

        $val = Yaml::parseFile('.gitlab-ci.yml');
        $yaml2 = Yaml::dump(['deploy_anywhere' => ['hahaha']]);
        $yaml = Yaml::dump($val, 5);


        $fileSystem->appendToFile('.git_test.yml', $yaml);
        $fileSystem->appendToFile('.git_test.yml', $yaml2);

        $output->writeln('Files created successfully!');

        return Command::SUCCESS;
    }

}